#include<iostream>
#include<stdio.h>
#include<opencv2/opencv.hpp>
#include<string>
#include<time.h>

using namespace std;
using namespace cv;

/**
   \fn video_fps(int width,int height)
   \brief Calcule le nombre de fps selon la resolution\n
   Copyright Derek Molloy, School of Electronic Engineering, Dublin City University\n
   www.derekmolloy.ie\n

   Redistribution and use in source and binary forms, with or without modification, are permitted\n
   provided that source code redistributions retain this notice.\n

   This software is provided AS IS and it comes with no warranties of any type.\n
 
   \param width
   largeur de la video
   \param height
   hauteur de la video
**/
double video_fps(int width,int height)
{
    VideoCapture capture(0);
    capture.set(CV_CAP_PROP_FRAME_WIDTH,width);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT,height);
    if(!capture.isOpened()){
	cout << "Failed to connect to the camera." << endl;
    }
    Mat frame, edges;
    
    for(int i=0;i<2;i++){
    	capture >>frame;
    	if(frame.empty()){
    		cout << "Failed to capture an image" << endl;
    	}
    }

    struct timespec start, end;
    clock_gettime(CLOCK_REALTIME, &start );

    int frames=10;
    for(int i=0; i<frames; i++){
    	capture >> frame;
    	if(frame.empty()){
		cout << "Failed to capture an image" << endl;
		return -1;
    	}
    	cvtColor(frame, edges, CV_BGR2GRAY);
    	Canny(edges, edges, 0, 30, 3);
    }

    clock_gettime( CLOCK_REALTIME, &end );
    double difference = (end.tv_sec - start.tv_sec) + (double)(end.tv_nsec - start.tv_nsec)/1000000000.0;
    cout << "It took " << difference << " seconds to process " << frames << " frames" << endl;
    cout << "Capturing and processing " << frames/difference << " frames per second " << endl;
    
    double fps= frames/difference;
    return fps;

};

/**
   \fn video( int resX, int resY,double fps)
   \brief Enregistre une video de 5 sec
   \param resX
   largeur de la video
   \param resY
   hauteur de la video
   \param fps
   Nombre de fps pour l'enregistrement
**/
void video( int resX, int resY,double fps){
	VideoCapture capture(0);
	capture.set(CV_CAP_PROP_FRAME_WIDTH,resX);
        capture.set(CV_CAP_PROP_FRAME_HEIGHT,resY);
	VideoWriter vid("capture-liv1.avi", CV_FOURCC('M','J','P','G'),fps,Size(resX,resY),true);
	int frames = fps*5;
        if(!capture.isOpened()){
	  cout << "Failed to connect to the camera." << endl;
	}
	for(int i=0;i<frames;i++){
	  Mat frame;
	  capture >> frame;
	  if(frame.empty()){
	   	cout << "Failed to capture an image" << endl;
          }
	  imshow("video",frame);
	  vid.write(frame);
	}
}

/**
\mainpage
Voici le travail pour le livrable 1 du Projet.

**/
int main(){

  int width=0;
  int height=0;
  cout<<"\n \n";
  cout<<"Available resolutions:\n";
  cout<<"176x144    160x120   320x176\n";
  cout<<"320x240    352x288   432x240\n";
  cout<<"800x600    866x480   960x544\n";
  cout<<"960x720    1184x656   1280x720\n";
  cout<<"1280x960\n \n";
  cout<<"What resolution  width  do you want?: ";
  cin>>width;
  cout<<"What resolution  heigth  do you want?: ";
  cin>>height;
  double fps = video_fps(width, height);
  //system("v4l2-ctl --list-devices > clef.txt");
  video(width, height, fps);

  return 0;
}

